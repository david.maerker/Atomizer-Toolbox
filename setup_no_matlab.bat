if not defined in_subprocess (cmd /k set in_subprocess=y ^& %0 %*) & exit )
@echo off
setlocal enabledelayedexpansion

REM Get the full path to the folder containing this batch script
set "script_dir=%~dp0"

echo Script Directory: %script_dir%

REM Check if the virtual environment folder exists
if not exist "%script_dir%\venv" (
    echo Creating virtual environment
    python -m venv "%script_dir%\venv"
)

REM Activate the virtual environment and install requirements
call "%script_dir%\venv\Scripts\activate.bat"
call "%script_dir%\venv\Scripts\pip.exe" install -r "%script_dir%\requirements_no_matlab.txt"

REM Run setup_uninstall.py
call "%script_dir%\venv\Scripts\python.exe" "%script_dir%\installer\setup_uninstall.py"

REM Run setup.py
call "%script_dir%\venv\Scripts\python.exe" "%script_dir%\setup.py %1"

REM Deactivate the virtual environment
call "%script_dir%\venv\Scripts\deactivate.bat"

echo Script execution completed
pause

endlocal
